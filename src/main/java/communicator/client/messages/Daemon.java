package communicator.client.messages;

import ch.qos.logback.classic.Logger;
import communicator.client.messages.controller.DisplayController;
import communicator.client.messages.controller.ServerConnection;
import communicator.client.users.model.Model;
import communicator.client.users.model.User;
import javafx.application.Platform;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.MultiUserChatManager;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by Krzysztof on 2016-01-13.
 * 17:04
 * Project: Communicator_Messages_2.
 */
public class Daemon {

    private final Logger logger = (Logger) LoggerFactory.getLogger(this.getClass().toString());
    private final ServerConnection serverConnection = new ServerConnection();
    private XMPPTCPConnection userConnection;
    private User me = Model.getInstance().getUser();
    private Stage primaryStage;
    private GridPane root;

    public Daemon() {
    }

    public Daemon(Stage primaryStage, GridPane root) {
        this.primaryStage = primaryStage;
        this.root = root;
    }

    private void ExceptionCaught(Exception ex) {

        DisplayController displayController = new DisplayController(primaryStage, root);
        displayController.displayErrorMsg();

        logger.error(ex.toString(), ex);
    }

    private void invitationSupport(MultiUserChat room) {

        logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());

        Platform.runLater(() -> {
            try {

                DisplayController displayController = new DisplayController(new Stage(), new GridPane(), userConnection, room);
                displayController.displayOnInvitation();
            } catch (SmackException.NotConnectedException | XMPPException.XMPPErrorException | SmackException.NoResponseException e) {
                logger.error(e.toString(), e);
            }
        });
    }

    public void setupListener() {

        logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());

        try {
            serverConnection.bind();
            userConnection = serverConnection.connect(me.getUsername(), me.getPassword());

            MultiUserChatManager manager = MultiUserChatManager.getInstanceFor(userConnection);

            manager.addInvitationListener((conn, room, inviter, reason, password, message) -> invitationSupport(room));

            DisplayController displayController = new DisplayController(new Stage(), new GridPane(), userConnection);
            displayController.display();

        } catch (IOException | SmackException | XMPPException ex) {
            ExceptionCaught(ex);
        }
    }

    public void invite(String user) {
        try {
//            serverConnection.bind();
//            userConnection = serverConnection.connect(user, user);

//            MultiUserChatManager manager = MultiUserChatManager.getInstanceFor(userConnection);

            DisplayController displayController = new DisplayController(new Stage(), new GridPane());
            displayController.display();

        } catch (SmackException ex) {
            ExceptionCaught(ex);
        }
    }
}

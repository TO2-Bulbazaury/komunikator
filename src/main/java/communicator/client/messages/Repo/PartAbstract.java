package communicator.client.messages.Repo;

import ch.qos.logback.classic.Logger;
import communicator.client.messages.IRepo.IPartAbstract;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by Maciej on 28.12.2015.
 * 17:05
 * Project: Communicator_Messages_2.
 */
abstract public class PartAbstract implements IPartAbstract {

    private final Logger logger = (Logger) LoggerFactory.getLogger(this.getClass().toString());

    abstract protected void loadFXML();

    abstract protected FXMLLoader getLoader();

    abstract protected Pane getPane();

    abstract protected void setPane(FXMLLoader loader) throws IOException;

    abstract protected Scene getScene();

    abstract protected void setScene(Pane connectionErrorPane);

    abstract protected Stage getPrimaryStage();

    abstract protected GridPane getRoot();

    private void addToRoot(Pane pane, int columnIndex, int rowIndex) {
        getRoot().add(pane, columnIndex, rowIndex);
    }

    protected void loadElements() { //TODO: updateSomeElements ??

    }

    protected void putObjectsIntoFXML() {

    }

    @Override
    public void show(int columnIndex, int rowIndex) {

        logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());

        Platform.runLater(() -> {
            try {
                loadFXML();
                setPane(getLoader());

                setScene(getPane());

                loadElements();
                putObjectsIntoFXML();

                addToRoot(getPane(), columnIndex, rowIndex);

                getPrimaryStage().show();
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
    }
}

package communicator.client.messages.controller;

import ch.qos.logback.classic.Logger;
import communicator.client.messages.MainController;
import communicator.client.messages.Repo.PartAbstract;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Maciej on 07.12.2015.
 * 17:05
 * Project: Communicator_Messages_2.
 */
public class DisplayController extends PartAbstract implements Initializable {

    private final Logger logger = (Logger) LoggerFactory.getLogger(this.getClass().toString());
    private XMPPTCPConnection userConnection;
    private MultiUserChat room;
    private Stage primaryStage;
    private GridPane root;
    private FXMLLoader loader;
    private Scene scene;

    public DisplayController() {
    }

    public DisplayController(Stage primaryStage, GridPane root) {
        this.primaryStage = primaryStage;
        this.root = root;
        show(0, 0);
    }

    public DisplayController(Stage primaryStage, GridPane root, XMPPTCPConnection userConnection) {
        primaryStage.setTitle(userConnection.getUser());
        this.primaryStage = primaryStage;
        this.root = root;
        this.userConnection = userConnection;
        show(0, 0);
    }

    public DisplayController(Stage primaryStage, GridPane root, XMPPTCPConnection userConnection, MultiUserChat room) {
        primaryStage.setTitle(room.toString());
        this.primaryStage = primaryStage;
        this.root = root;
        this.userConnection = userConnection;
        this.room = room;
        show(0, 0);
    }

    @Override
    protected void loadFXML() {
        loader = new FXMLLoader();
        loader.setLocation(MainController.class.getResource("/DisplayView.fxml"));
    }

    @Override
    protected FXMLLoader getLoader() {
        return loader;
    }

    @Override
    protected Pane getPane() {
        return root;
    }

    @Override
    protected void setPane(FXMLLoader loader) throws IOException {
        root = loader.load();
    }

    @Override
    protected Scene getScene() {
        return scene;
    }

    @Override
    protected void setScene(Pane pane) {
        scene = new Scene(pane);
    }

    @Override
    protected Stage getPrimaryStage() {
        return primaryStage;
    }

    @Override
    protected GridPane getRoot() {
        return root;
    }

    @Override
    public void show(int columnIndex, int rowIndex) {

        logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());

        try {
            loadFXML();

            setPane(getLoader());

            setScene(root);

            primaryStage = getPrimaryStage();

            Platform.runLater(() -> {
                primaryStage.setScene(getScene());
                primaryStage.show();
            });

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
        }
    }

    public void displayErrorMsg() {
        ConnectionErrorController connectionErrorController = new ConnectionErrorController(primaryStage, root);
        connectionErrorController.show(0, 0);
    }

    public void displayOnInvitation() throws SmackException.NotConnectedException, XMPPException.XMPPErrorException, SmackException.NoResponseException {

        logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());

        ConversationController conversationController = new ConversationController(primaryStage, root, userConnection, room);
//        conversationController.joinConversation();


        LoggedListController loggedListController = new LoggedListController(primaryStage, root, room);
        loggedListController.show(0, 2);
    }

    public void display() throws SmackException {

        logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());

        //            ConversationController conversationController = new ConversationController(primaryStage, root, userConnection, room);
        ConversationController conversationController = new ConversationController(primaryStage, root);
        MessageController messageController = new MessageController(primaryStage, root, conversationController);
//            MultiUserChat room = conversationController.newConversation();
        //conversationController.newConversation();


        LoggedListController loggedListController = new LoggedListController(primaryStage, root, room);
        loggedListController.show(0, 2);
    }

    @FXML
    public void initialize(URL location, ResourceBundle resources) {

    }

}

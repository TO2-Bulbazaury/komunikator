package communicator.client.messages.controller;

import ch.qos.logback.classic.Logger;
import communicator.client.messages.MainController;
import communicator.client.messages.Repo.PartAbstract;
import communicator.client.users.model.Model;
import communicator.client.users.model.User;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.MultiUserChatManager;
import org.jivesoftware.smackx.xdata.Form;
import org.jivesoftware.smackx.xdata.FormField;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.UUID;

/**
 * Created by Maciej on 07.12.2015.
 * 17:06
 * Project: Communicator_Messages_2.
 */
public class ConversationController extends PartAbstract implements Initializable {

    private static final HtmlBuilder htmlBuilder = new HtmlBuilder();
    private final Logger logger = (Logger) LoggerFactory.getLogger(this.getClass().toString());
    private XMPPTCPConnection connection;
    private MultiUserChat room;
    private MessageListener listener;
    private Stage primaryStage;
    private GridPane root;
    private Scene scene;
    private String currentContent;
    private FXMLLoader loader;
    private WebEngine allMessagesWebEngine;
    private ObservableList<Node> elements;
    private User me = Model.getInstance().getUser();

    @FXML
    private WebView allMessagesWebView;

    @FXML
    private Pane conversationPane;

    public ConversationController() {
    }

    public ConversationController(Stage primaryStage, GridPane root) {
        this.primaryStage = primaryStage;
        this.root = root;
        show(0, 0);
        //MessageController messageController = new MessageController(primaryStage, root, allMessagesWebEngine);
    }

    public ConversationController(Stage primaryStage, GridPane root, XMPPTCPConnection connection, MultiUserChat room) {
        this.primaryStage = primaryStage;
        this.root = root;
        this.connection = connection;
        this.room = room;
        this.currentContent = htmlBuilder.loadConversationHeader();
        show(0, 0);
    }

    @Override
    protected void loadFXML() {
        loader = new FXMLLoader();
        loader.setLocation(MainController.class.getResource("/ConversationView.fxml"));
    }

    @Override
    protected FXMLLoader getLoader() {
        return loader;
    }

    @Override
    protected Pane getPane() {
        return conversationPane;
    }

    @Override
    protected void setPane(FXMLLoader loader) throws IOException {
        conversationPane = loader.load();
    }

    @Override
    protected Scene getScene() {
        return scene;
    }

    @Override
    protected void setScene(Pane pane) {
        scene = new Scene(pane);
    }

    @Override
    protected Stage getPrimaryStage() {
        return primaryStage;
    }

    @Override
    protected GridPane getRoot() {
        return root;
    }

    @Override
    protected void loadElements() {

        logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());

        elements = conversationPane.getChildren();
        allMessagesWebView = (WebView) elements.get(0);
        allMessagesWebEngine = allMessagesWebView.getEngine();
    }

    public void getMessage(String message) {

        logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());

        //if (message.getType() == Message.Type.chat || message.getType() == Message.Type.normal || message.getType() == Message.Type.groupchat) {
            //if (message.getBody() != null) {
                Platform.runLater(() -> {
                    //currentContent += htmlBuilder.loadMessageStructure(message);
                    allMessagesWebEngine.loadContent(message);
                });
           // }
        //}
    }

//    public void messageListenerOnChat(MultiUserChat chat) {
//
//        logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
//
//        chat.addMessageListener(message -> getMessage(message));
//    }
/*
    public MultiUserChat newConversation() throws SmackException, XMPPException.XMPPErrorException {

        logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());


        MultiUserChat chat = createChat();

        chat.join(me.getUsername());

        messageListenerOnChat(chat);

        MessageController messageController = new MessageController(primaryStage, root, chat);

        return chat;
    }
    */

    public void newConversation() {

    }

//    public void joinConversation() throws SmackException.NotConnectedException, XMPPException.XMPPErrorException, SmackException.NoResponseException {
//
//        try {
//            logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
//
//            //MultiUserChatManager manager = MultiUserChatManager.getInstanceFor(connection);
//            logger.info(room.toString());
//
////            room.getConfigurationForm();
//            room.join(me.getUsername());
//            messageListenerOnChat(room);
//
//            MessageController messageController = new MessageController(primaryStage, root, room);
//        } catch (SmackException.NoResponseException | XMPPException.XMPPErrorException | SmackException.NotConnectedException e) {
//            e.printStackTrace();
//        }
//    }

    private MultiUserChat createChat() throws XMPPException.XMPPErrorException, SmackException {
        String uniqueID = UUID.randomUUID().toString();
        MultiUserChatManager manager = MultiUserChatManager.getInstanceFor(connection);
        MultiUserChat chat = manager.getMultiUserChat(uniqueID + "@conference.makowka");


        chat.create(me.getUsername());


        Form form = chat.getConfigurationForm();
        Form submitForm = form.createAnswerForm();
        // submitForm.setAnswer("muc#roomconfig_publicroom", true);
        for (Iterator<FormField> fields = form.getFields().iterator(); fields.hasNext();) {
            FormField field = (FormField) fields.next();
            logger.info(field.getLabel());

            if (!FormField.FORM_TYPE.equals(field.getType())
                    && field.getVariable() != null) {
                submitForm.setDefaultAnswer(field.getVariable());
            }
        }


        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        submitForm.setAnswer("muc#roomconfig_allowinvites", true);
        submitForm.setAnswer("muc#roomconfig_persistentroom", true);
        chat.sendConfigurationForm(submitForm);



//        Form f = chat.getConfigurationForm();
        //submitForm.setAnswer("muc#roomconfig_roomname", room);
        //setRoomConfig(chat);

        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            chat.destroy("shutdown", "");
                        } catch (SmackException.NoResponseException | XMPPException.XMPPErrorException | SmackException.NotConnectedException e) {
                            e.printStackTrace();
                        }
                    }
                })
        );

        return chat;
    }

    @FXML
    public void initialize(URL location, ResourceBundle resources) {

    }
}

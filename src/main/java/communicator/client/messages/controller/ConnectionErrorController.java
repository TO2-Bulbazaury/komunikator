package communicator.client.messages.controller;

import ch.qos.logback.classic.Logger;
import communicator.client.messages.MainController;
import communicator.client.messages.Repo.PartAbstract;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by Maciej on 22.12.2015.
 * 17:06
 * Project: Communicator_Messages_2.
 */
public class ConnectionErrorController extends PartAbstract {

    private final Logger logger = (Logger) LoggerFactory.getLogger(this.getClass().toString());
    private FXMLLoader loader;
    private Stage primaryStage;
    private GridPane root;
    private Scene scene;

    @FXML
    private Pane connectionErrorPane;

    @FXML
    private Label connectionErrorLabel;

    public ConnectionErrorController() {
    }

    public ConnectionErrorController(Stage primaryStage, GridPane root) {
        this.primaryStage = primaryStage;
        this.root = root;
        show(0, 0);
    }

    @Override
    protected void loadFXML() {
        loader = new FXMLLoader();
        loader.setLocation(MainController.class.getResource("/ConnectionErrorView.fxml"));
    }

    @Override
    protected FXMLLoader getLoader() {
        return loader;
    }

    @Override
    protected Pane getPane() {
        return connectionErrorPane;
    }

    @Override
    protected void setPane(FXMLLoader loader) throws IOException {
        connectionErrorPane = loader.load();
    }

    @Override
    protected Scene getScene() {
        return scene;
    }

    @Override
    protected void setScene(Pane connectionErrorPane) {
        scene = new Scene(connectionErrorPane);
    }

    @Override
    protected Stage getPrimaryStage() {
        return primaryStage;
    }

    @Override
    protected GridPane getRoot() {
        return root;
    }

}

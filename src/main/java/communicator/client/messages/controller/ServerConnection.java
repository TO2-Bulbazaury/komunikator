package communicator.client.messages.controller;

import ch.qos.logback.classic.Logger;
import communicator.client.users.Main;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Maciej on 15.12.2015.
 * 17:05
 * Project: Communicator_Messages_2.
 */
public class ServerConnection {

    private final Logger logger = (Logger) LoggerFactory.getLogger(this.getClass().toString());
    private String serverAddress;
    private String serviceName;
    private int serverPort;
    private boolean debugMode;
    private static XMPPTCPConnectionConfiguration.Builder config;
    private Properties properties;
    private XMPPTCPConnection connection;

    public ServerConnection() {
    }

    private void loadProperties() {

        logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());

        try {
            InputStream inputStream = Main.class.getResourceAsStream("/config.properties");;
            properties = new Properties();
            properties.load(inputStream);
        } catch (IOException ex) {
            logger.error(ex.toString(), ex);
        }
    }

    private void createConnectionWithTheServer(Properties properties) {

        logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());

        serverAddress = properties.getProperty("serverAddress");
        logger.info("serverAddress: {}", serverAddress);
        serviceName = properties.getProperty("serviceName");
        logger.info("serviceName: {}", serviceName);
        serverPort = Integer.parseInt(properties.getProperty("serverPort"));
        logger.info("serverPort: {}", serverPort);
        debugMode = Boolean.parseBoolean(properties.getProperty("debug"));
        logger.info("debug: {}", debugMode);

    }

    public XMPPTCPConnectionConfiguration.Builder bind() {

        logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());

        loadProperties();

        createConnectionWithTheServer(properties);
        config = XMPPTCPConnectionConfiguration.builder();

        config.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled)
                .setServiceName(serviceName)
                .setHost(serverAddress)
                .setPort(serverPort)
                .setDebuggerEnabled(debugMode);

        return config;
    }

    public XMPPTCPConnection connect(String userName, String password) throws IOException, SmackException, XMPPException {

        logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());

        config.setUsernameAndPassword(userName, password);
        connection = new XMPPTCPConnection(config.build());
        connection.connect().login();

        return connection;
    }

    public void disconnect() {
        connection.disconnect();
    }

}

package communicator.client.messages.controller;

import communicator.client.messages.Repo.PartAbstract;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by Jakub Janusz on 2016-01-20.
 */
public class Buttons extends PartAbstract {
    @Override
    protected void loadFXML() {

    }

    @Override
    protected FXMLLoader getLoader() {
        return null;
    }

    @Override
    protected Pane getPane() {
        return null;
    }

    @Override
    protected void setPane(FXMLLoader loader) throws IOException {

    }

    @Override
    protected Scene getScene() {
        return null;
    }

    @Override
    protected void setScene(Pane connectionErrorPane) {

    }

    @Override
    protected Stage getPrimaryStage() {
        return null;
    }

    @Override
    protected GridPane getRoot() {
        return null;
    }
}

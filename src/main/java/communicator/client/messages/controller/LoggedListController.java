package communicator.client.messages.controller;

import ch.qos.logback.classic.Logger;
import communicator.client.messages.MainController;
import communicator.client.messages.Repo.PartAbstract;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Krzysztof Węgrzyński on 2015-12-15.
 * 17:05
 * Project: Communicator_Messages_2.
 */
public class LoggedListController extends PartAbstract implements Initializable {

    private final Logger logger = (Logger) LoggerFactory.getLogger(this.getClass().toString());
    private static FXMLLoader loader;
    private GridPane root;
    private Stage primaryStage;
    private MultiUserChat chat;
    private ObservableList<String> loggedUsers = FXCollections.observableArrayList();
    private Scene scene;

    @FXML
    private Pane loggedListPane;

    @FXML
    private ToolBar loggedListToolBar;

    @FXML
    private ComboBox<String> loggedComboBox = new ComboBox<>();

    public LoggedListController() {
    }

    public LoggedListController(Stage primaryStage, GridPane root) {
        this.primaryStage = primaryStage;
        this.root = root;
    }

    public LoggedListController(Stage primaryStage, GridPane root, MultiUserChat room) {
        this.primaryStage = primaryStage;
        this.root = root;
        this.chat = room;
    }

    @Override
    protected void loadFXML() {
        loader = new FXMLLoader();
        loader.setLocation(MainController.class.getResource("/LoggedListView.fxml"));
    }

    @Override
    protected FXMLLoader getLoader() {
        return loader;
    }

    @Override
    protected Pane getPane() {
        return loggedListPane;
    }

    @Override
    protected void setPane(FXMLLoader loader) throws IOException {
        loggedListPane = loader.load();
    }

    @Override
    protected Scene getScene() {
        return scene;
    }

    @Override
    protected void setScene(Pane pane) {
        scene = new Scene(pane);
    }

    @Override
    protected Stage getPrimaryStage() {
        return primaryStage;
    }

    @Override
    protected GridPane getRoot() {
        return root;
    }

    @Override
    protected void putObjectsIntoFXML() {
        LoggedListController controller = loader.<LoggedListController>getController();
        controller.setChat(chat);
    }

    public MultiUserChat getChat() {
        return chat;
    }

    public void setChat(MultiUserChat chat) {
        this.chat = chat;
    }

    private void sendInvitation(String value) throws SmackException.NotConnectedException {
        chat.invite("jacek@makowka/Smack", "das");
    }

    @FXML
    public void updateLoggedUsers() {

        logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());

        loggedUsers.setAll("jacek", "Wacek", "Kasztan");
        loggedComboBox.setItems(loggedUsers);

    }

    @FXML
    public void addUserToConversation() throws SmackException.NotConnectedException {

        logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());

        if (!loggedComboBox.getValue().equals("użytkownicy")) {
            sendInvitation(loggedComboBox.getValue());
        }
    }

    @FXML
    public void correctDisplay() {
        loggedComboBox.setValue("użytkownicy");
    }

    @FXML
    public void initialize(URL url, ResourceBundle resourceBundle) {
        correctDisplay();
    }
}

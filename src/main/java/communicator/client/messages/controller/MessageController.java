package communicator.client.messages.controller;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.joran.action.ConfigurationAction;
import communicator.client.messages.MainController;
import communicator.client.messages.Repo.PartAbstract;
import communicator.client.users.Plugin.Plugin;
import communicator.client.users.model.Model;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Created by Maciej on 07.12.2015.
 * 17:05
 * Project: Communicator_Messages_2.
 */

public class MessageController extends PartAbstract implements Initializable {

    private static FXMLLoader loader;
    private final Logger logger = (Logger) LoggerFactory.getLogger(this.getClass().toString());
    private ConversationController conversationController;
    private MultiUserChat chat;
    private GridPane root;
    private Stage primaryStage;
    private Scene scene;

    @FXML
    private Pane messagePane;

    @FXML
    private TextField newMessageComposingTextField = new TextField();

    @FXML
    private Button sendNewMessageButton;

    public MessageController() {
    }

    public MessageController(Stage primaryStage, GridPane root, ConversationController conversationController) {
        this.primaryStage = primaryStage;
        this.root = root;
        this.conversationController = conversationController;
        show(0, 1);
    }

    public MessageController(Stage primaryStage, GridPane root, MultiUserChat chat) {
        this.primaryStage = primaryStage;
        this.root = root;
        this.chat = chat;
        show(0, 1);
    }

    @Override
    protected void loadFXML() {
        loader = new FXMLLoader();
        loader.setLocation(MainController.class.getResource("/MessageView.fxml"));
    }

    @Override
    protected FXMLLoader getLoader() {
        return loader;
    }

    @Override
    protected Pane getPane() {
        return messagePane;
    }

    @Override
    protected void setPane(FXMLLoader loader) throws IOException {
        messagePane = loader.load();
    }

    @Override
    protected Scene getScene() {
        return scene;
    }

    @Override
    protected void setScene(Pane pane) {
        scene = new Scene(pane);
    }

    @Override
    protected Stage getPrimaryStage() {
        return primaryStage;
    }

    @Override
    protected GridPane getRoot() {
        return root;
    }

    @Override
    protected void putObjectsIntoFXML() {
        MessageController controller = getLoader().<MessageController>getController();
        controller.setController(conversationController);
    }

    public MultiUserChat getChat() {
        return chat;
    }

    public void setChat(MultiUserChat chat) {
        this.chat = chat;
    }

    public void setController(ConversationController controller){
        this.conversationController = controller;
    }

    @FXML
    private void sendOnMouseClicked() {
        sendMessage();
    }

    @FXML
    private void sendMessageOnEnter() {
        sendMessage();
    }

    public void sendMessage() {

        logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());

        String text = newMessageComposingTextField.getText();

        ArrayList<Plugin> plugins = Model.getInstance().getPluginManager().getActivePlugins();

        for(Plugin plugin : plugins) {
            text = plugin.parse(text);
        }

        //chat.sendMessage(text);
        conversationController.getMessage(text);
        newMessageComposingTextField.setText("");

    }



    @FXML
    public void initialize(URL location, ResourceBundle resources) {

    }

}

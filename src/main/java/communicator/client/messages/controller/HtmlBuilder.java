package communicator.client.messages.controller;

import ch.qos.logback.classic.Logger;
import org.antlr.stringtemplate.StringTemplate;
import org.antlr.stringtemplate.StringTemplateGroup;
import org.antlr.stringtemplate.language.DefaultTemplateLexer;
import org.jivesoftware.smack.packet.Message;
import org.slf4j.LoggerFactory;

/**
 * Created by Maciej on 16.12.2015.
 * 17:05
 * Project: Communicator_Messages_2.
 */

public class HtmlBuilder {

    private final Logger logger = (Logger) LoggerFactory.getLogger(this.getClass().toString());
    private String rootDir;

    public HtmlBuilder() {
        this.rootDir = System.getProperty("user.dir");
    }

    private StringTemplate loadStringTemplate(String template) {

        logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());

        StringTemplateGroup group = new StringTemplateGroup(template, rootDir + "/templates", DefaultTemplateLexer.class);
//        System.out.println(group.getRootDir());

        return group.getInstanceOf(template);
    }


    public String loadConversationHeader() {

        logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());

        StringTemplate conversationHeader = loadStringTemplate("conversationHeader");

        return conversationHeader.toString();
    }

    public String loadMessageStructure(Message message) {

        logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());

        StringTemplate messageStructure = loadStringTemplate("messageStructure");

        messageStructure.setAttribute("messageSender", message.getFrom());
        messageStructure.setAttribute("messageBody", message.getBody());

        return messageStructure.toString();
    }
}

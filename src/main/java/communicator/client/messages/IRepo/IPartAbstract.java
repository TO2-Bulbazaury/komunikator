package communicator.client.messages.IRepo;

/**
 * Created by Maciej on 28.12.2015.
 * 17:05
 * Project: Communicator_Messages_2.
 */
public interface IPartAbstract {

    public void show(int columnIndex, int rowIndex);

}

package communicator.client.messages;

import ch.qos.logback.classic.Logger;
import communicator.client.messages.controller.DisplayController;
import javafx.application.Application;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.slf4j.LoggerFactory;

public class MainController extends Thread {

    private final Logger logger = (Logger) LoggerFactory.getLogger(this.getClass().toString());
    private Stage primaryStage;
    private Daemon daemon;

    public void run() {

        logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());

        daemon = new Daemon(primaryStage, new GridPane());
//        daemon.setupListener();

    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public void invite(String user) {
        daemon.invite(user);
    }
}

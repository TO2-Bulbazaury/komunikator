package communicator.client.users.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by Jakub Janusz on 2015-12-09.
 */
public class ServerReceiver extends Thread {

    private Socket server;
    private BufferedReader reader;
    private final String SEPARATOR = "#$#";
    private Logger logger;
    private CountDownLatch waiter;

    public ServerReceiver(Socket server, CountDownLatch waiter) throws IOException {
        this.logger = LoggerFactory.getLogger(this.getClass().toString());
        this.waiter = waiter;
        this.server = server;
        try {
            this.reader = new BufferedReader(new InputStreamReader(server.getInputStream()));
        } catch (NullPointerException e) {
            logger.error(e.toString());
        }
    }

    public void run() {
        receiveResponses();
    }

    public void setWaiter(CountDownLatch waiter) {
        this.waiter = waiter;
    }

    public void receiveResponses() {
        String response;
        while(true) {
            try {
                if(reader.ready()) {
                    response = reader.readLine();
                    logger.info(response);
                    System.out.println(response);
                    logger.info("Received following response from {}:\n{}", server, response);
                    parseResponse(response);
                }
            } catch (NullPointerException | IOException e) {
                logger.error(e.toString());
            }
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                logger.error(e.toString());
            }
        }
    }

    public void parseResponse(String response) {
        /* TODO
        * autoryzacja - A
        * rejestracja - R
        * usuwanie konta - D
        * pobranie aktywnych u�ytkownikow - U
        * pobranie danych profilowych - P
        * edycja danych profilowych - E
        * dezaktywacja konta - X
        * aktwacja konta - Y
        * ban - B
        * */
        String[] parts = response.split("[#][$][#]");
        String type = parts[0];
        switch (type) {
            case "A":
                if (parts.length != 2) {
                    logger.warn("parseResponse - Incorrect number of arguments for request of A type.");
                } else {
                    Model.getInstance().setToken(Integer.parseInt(parts[1]));
                    if(Integer.parseInt(parts[1]) == 0)
                        logger.warn("parseResponse - Received wrong session token.");
                }
                break;
            case "R":
                if (parts.length != 2) {
                    logger.warn("parseResponse - Incorrect number of arguments for request of R type.");
                } else {
                    switch (parts[1]) {
                        case "acc":
                            Model.getInstance().setRegistrationFlag(true);
                            break;
                        case "rej":
                            Model.getInstance().setRegistrationFailureFlag(true);
                            break;
                        default:
                            logger.warn("parseResponse - Incorrect response message of type R.");
                            break;
                    }
                }
                break;
            case "U":
                if(parts.length < 2) {
                    logger.warn("parseResponse = Incorrect number of arguments foe request of U type.");
                } else {
                    List<String> users = new LinkedList<>();
                    for(int i = 1; i < parts.length; i++) {
                        users.add(parts[i]);
                    }
                    Model.getInstance().setUsersOnline(users);
                    Model.getInstance().setUsersFlag(true);
                }
                break;
            case "P":
                if(parts.length != 5) {
                    if(parts.length == 1) {
                        Model.getInstance().setProfileFlag(true);
                        logger.warn("parseResponse - Empty response for request of P type.");
                    } else {
                        logger.warn("parseResponse = Incorrect number of arguments for request of U type.");
                    }
                } else {
                    ProfileData profileData = new ProfileData();
                    profileData.setEmail(parts[1]);
                    profileData.setName(parts[2]);
                    profileData.setAge(parts[3]);
                    profileData.setCity(parts[4]);
                    Model.getInstance().setSomeProfileData(profileData);
                    Model.getInstance().setProfileFlag(true);
                }
                break;
            default:
                logger.warn("parseResponse - Request type does no match any of possible types.");
                break;
        }
        this.waiter.countDown();
    }

}

package communicator.client.users.model;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Dawid on 2015-12-05.
 */
public class User {

    private long id;
    private String username;
    private String password;
    private Set<Preference> preferences;
    private ProfileData profileData;

    public User() {
        this.preferences = new HashSet<>();
        this.profileData = new ProfileData();
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.preferences = new HashSet<>();
        this.profileData = new ProfileData();
    }

    private long getId() {
        return this.id;
    }

    private void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Preference> getPreferences() {
        return this.preferences;
    }

    private void setPreferences(Set<Preference> preferences) {
        this.preferences = preferences;
    }

    public void addPreference(Preference preference) {
        this.preferences.add(preference);
    }

    public ProfileData getProfileData() {
        return this.profileData;
    }

    public void setProfileData(ProfileData profileData) {
        this.profileData = profileData;
    }

    /*
    public String toString() {
        return this.username + "\t" + Encryption.encrypt(this.password);
    }
    */
}

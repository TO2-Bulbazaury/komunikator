package communicator.client.users.model;

import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

import static communicator.client.users.persistence.HibernateUtils.session;

/**
 * Created by Dawid on 2015-12-05.
 */
public class DatabaseConnector {

    public DatabaseConnector() {

    }

    public void savePreferences(User user) {
        Session session = session();
        Transaction transaction = session.beginTransaction();

        session.persist(user);

        for (Preference preference : user.getPreferences()) {
            session.persist(preference);
        }

        transaction.commit();
        session.close();
    }

    public List<Preference> loadPreferences(User user) {
        Session session = session();

        List<Preference> result = session.createQuery("select p from Preference p join p.user u where u.username like :username").setParameter("username", user.getUsername()).list();

        session.close();

        return result;
    }

}

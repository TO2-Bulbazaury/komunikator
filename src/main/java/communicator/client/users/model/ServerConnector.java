package communicator.client.users.model;

import communicator.client.users.Main;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import static communicator.client.users.persistence.HibernateUtils.session;

/**
 * Created by Dawid on 2015-12-05.
 */
public class ServerConnector {

    private Socket server;
    private ServerReceiver receiver;
    private ServerSender sender;
    private Logger logger;
    private CountDownLatch waiter;

    public ServerConnector(CountDownLatch waiter) {
        this.waiter = waiter;
        listenSocket();
    }

    private void listenSocket(){
        Properties properties = new Properties();
        this.logger = LoggerFactory.getLogger(this.getClass().toString());
        try {
            loadProperties(properties);
            connectToServer(properties);
            this.receiver = new ServerReceiver(server, waiter);
            this.sender = new ServerSender(server);
        } catch (IOException e) {
            logger.error(e.toString());
        }
        receiver.start();
        sender.start();
    }

    public void loadProperties(Properties properties) throws IOException {
        InputStream inputStream = Main.class.getResourceAsStream("/config.properties");
        properties.load(inputStream);
    }

    public void connectToServer(Properties properties) {
        String serverAddress = properties.getProperty("serverAddress");
        int serverPort = Integer.parseInt(properties.getProperty("socketPort"));
        try {
            server = new Socket(serverAddress, serverPort);
            logger.info("Connecting to server {}", server);
        } catch (IOException e) {
            logger.error(e.toString());
        }
    }

    public boolean sendNewUser(){

        return true;
    }

    public boolean checkCredentials(String username, String password) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        Session session = session();
        Transaction transaction = session.getTransaction();

        session.persist(user);

        transaction.commit();
        session.close();

        return true;
    }

    public void setWaiter(CountDownLatch waiter) {
        this.waiter = waiter;
        this.receiver.setWaiter(waiter);
    }

    public void authorize(User user) {
        this.sender.sendRequest(user, "", "A", "");
    }

    public void register(User user, String token) {
        this.sender.sendRequest(user, token, "R", "");
    }

    public void getUsersList(User user, String token) {
        this.sender.sendRequest(user, token, "U", "");
    }

    public void getProfileData(User user, String token) {
        this.sender.sendRequest(user, token, "P", "");
    }

}

package communicator.client.users.model;

/**
 * Created by Dawid on 2015-12-05.
 */
public class Preference {

    private long id;
    private String name;
    private boolean choice;
    private User user;

    public Preference() {

    }

    public Preference(User user,String name, boolean choice) {
        this.user = user;
        this.name = name;
        this.choice = choice;
    }

    private long getId() {
        return this.id;
    }

    private void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getChoice() {
        return this.choice;
    }

    public void setChoice(boolean choice) {
        this.choice = choice;
    }

    public User getUser() {
        return this.user;
    }

    private void setUser(User user) {
        this.user = user;
    }

}

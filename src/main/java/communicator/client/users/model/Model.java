package communicator.client.users.model;

import ch.qos.logback.classic.Logger;
import communicator.client.messages.MainController;
import communicator.client.users.Plugin.PluginManager;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by Dawid on 2015-12-05.
 */
public class Model {

    private static Model ourInstance = new Model();
    private MainController mainController;
    private PluginManager pluginManager;
    private User user;
    private DatabaseConnector databaseConnector;
    private Encryption encryption;
    private ServerConnector serverConnector;
    private boolean session;
    private int token;
    private Logger logger;
    private boolean registrationFlag;
    private boolean registrationFailureFlag;
    private CountDownLatch waiter;
    private List<String> usersOnline;
    private boolean usersFlag;
    private ProfileData someProfileData;
    private boolean profileFlag;

    private Model() {
        this.mainController = new MainController();
        this.pluginManager = new PluginManager("plugins");
        this.waiter = new CountDownLatch(1);
        this.databaseConnector = new DatabaseConnector();
        this.encryption = new CaesarEncryption();
        this.serverConnector = new ServerConnector(waiter);
        this.logger = (Logger) LoggerFactory.getLogger(this.getClass().toString());
        this.usersOnline = new LinkedList<>();
    }

    public static Model getInstance() {
        return ourInstance;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return this.user;
    }

    public String getUserName() {
        return this.user.getUsername();
    }

    public void setToken(int token) {
        this.token = token;
    }

    public void setRegistrationFlag(boolean flag) {
        this.registrationFlag = flag;
    }

    public void setRegistrationFailureFlag(boolean flag) {
        this.registrationFailureFlag = flag;
    }

    public void setUsersFlag(boolean flag) {
        this.usersFlag = flag;
    }

    public void setProfileFlag(boolean flag) {
        this.profileFlag = flag;
    }

    public DatabaseConnector getDatabaseConnector() {
        return this.databaseConnector;
    }

    public Encryption getEncryption() {
        return this.encryption;
    }

    public ServerConnector getServerConnector() {
        return this.serverConnector;
    }

    public void setUsersOnline(List<String> usersOnline) {
        this.usersOnline = usersOnline;
    }

    public void setSomeProfileData(ProfileData profileData) {
        this.someProfileData = profileData;
    }

    public MainController getMainController() {
        return this.mainController;
    }

    public PluginManager getPluginManager() {
        return this.pluginManager;
    }

    public void newConversationNotify(String partnerUserName) {
        this.mainController.invite(partnerUserName);
    }

    public void archiveRequestNotifty(){
        logger.info("Notifying plugin manager.");
    }

    public void sendPreferencess(String string){
        logger.info("Sending preferences to plugin module.");
    }

    public void initWaiter() {
        this.waiter = new CountDownLatch(1);
        serverConnector.setWaiter(waiter);
    }

    public boolean authorize() {
        this.token = -1;
        this.serverConnector.authorize(user);

        while(this.token == -1) {
            try {
                this.waiter.await();
            } catch (InterruptedException e) {
                logger.error(e.toString());
            }
        }

        this.initWaiter();

        return this.token != 0;
    }

    public boolean register() {
        this.registrationFailureFlag = false;
        this.registrationFlag = false;
        this.serverConnector.register(user, Integer.toString(token));

        while(!registrationFailureFlag && !registrationFlag) {
            try {
               this.waiter.await();
            } catch (InterruptedException e) {
                logger.error(e.toString());
            }
        }

        this.initWaiter();

        return !registrationFailureFlag;
    }

    public List<String> getUsersList() {
        this.usersFlag = false;
        this.serverConnector.getUsersList(this.user, Integer.toString(token));

        while(!usersFlag) {
            try {
                this.waiter.await();
            } catch (InterruptedException e) {
                logger.error(e.toString());
            }
        }

        this.initWaiter();

        return usersOnline;
    }

    public ProfileData getProfileData(String username) {
        this.profileFlag= false;
        this.serverConnector.getProfileData(new User(username, ""), Integer.toString(this.token));

        while(!profileFlag) {
            try {
                this.waiter.await();
            } catch (InterruptedException e) {
                logger.error(e.toString());
            }
        }

        this.initWaiter();

        return someProfileData;
    }

    public static boolean endSession(){
        return true;
    }

    public void logout(){
        setUser(null);
        endSession();
    }

    public int deleteUser(String pass){

        return 0;
    }

    public int deactivateUser(String pass){

        return 0;
    }

}

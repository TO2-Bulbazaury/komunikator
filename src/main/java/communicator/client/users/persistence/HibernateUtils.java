package communicator.client.users.persistence;

import communicator.client.users.Main;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public final class HibernateUtils {

    private HibernateUtils() {

    }

	private static SessionFactory sessionFactory;
	
	static
	{
        Configuration configuration = new Configuration();
		configuration.configure(Main.class.getResource("/hibernate.cfg.xml"));
		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
				configuration.getProperties()).build();
		sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	}
	
	public static SessionFactory sessionFactory() {
		return sessionFactory;
	}
	
	public static Session session() {
		return sessionFactory().openSession();
	}
	
	public static void shutdown() {
		sessionFactory().close();
	}
}

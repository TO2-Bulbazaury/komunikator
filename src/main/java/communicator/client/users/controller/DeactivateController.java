package communicator.client.users.controller;

import ch.qos.logback.classic.Logger;
import communicator.client.users.Main;
import communicator.client.users.model.Model;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by Dawid on 2015-12-05.
 */
public class DeactivateController implements Controller {
    private Stage primaryStage;
    private Stage deactivateStage;
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass().toString());

    public DeactivateController(){}

    @Override
    public void show() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/Deactivate.fxml"));
            loader.setController(this);
            BorderPane page = (BorderPane) loader.load();

            deactivateStage = new Stage();
            deactivateStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    Overseer.getInstance().closingWindow(new DeactivateController());
                }
            });
            deactivateStage.setTitle("Account options");
            deactivateStage.initModality(Modality.WINDOW_MODAL);
            deactivateStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            deactivateStage.setScene(scene);


            deactivateStage.showAndWait();

        } catch (IOException e) {
            logger.error(e.toString());
        }
    }

    @Override
    public Stage getStage() {
        return deactivateStage;
    }

    private boolean checkPass(String pass){
        return Model.getInstance().getEncryption().encrypt(pass).equals(Model.getInstance().getUser().getPassword());
    }

    public void initialize(){
        errorLabelDelete.setTextFill(Color.RED);
        errorLabelDeactivate.setTextFill(Color.RED);
    }

    @FXML
    private Button submitDeactivate;
    @FXML
    private Button submitDelete;
    @FXML
    private PasswordField deactivatePasswordField;
    @FXML
    private PasswordField deletePasswordField;
    @FXML
    private Label errorLabelDelete;
    @FXML
    private Label errorLabelDeactivate;

    @FXML
    private void handleDeactivateSubmitAction(ActionEvent event){
        if(checkPass(deactivatePasswordField.getText())){
            deactivatePasswordField.setStyle("-fx-text-box-border: gray;");
            if(Model.getInstance().deactivateUser(deactivatePasswordField.getText())>0){
                errorLabelDeactivate.setText("There was a problem with deleting your account. Please try again later");
            }
            else{
                errorLabelDeactivate.setText("");
            }
        }
        else{
            deactivatePasswordField.setStyle("-fx-text-box-border: red;");
            if(deactivatePasswordField.getText().isEmpty()){
                errorLabelDeactivate.setText("You have to type in your password");
            }
            else{
                errorLabelDeactivate.setText("Provided password is incorrect");
            }
        }
    }
    @FXML
    private void handleDeleteSubmitAction(ActionEvent event){
        if(checkPass(deletePasswordField.getText())){
            deletePasswordField.setStyle("-fx-text-box-border: gray;");
            if(Model.getInstance().deleteUser(deactivatePasswordField.getText())>0){
                errorLabelDelete.setText("There was a problem with deleting your account. Please try again later");
            }
            else{
                errorLabelDelete.setText("");
            }
        }
        else{
            deletePasswordField.setStyle("-fx-text-box-border: red;");
            if(deletePasswordField.getText().isEmpty()){
                errorLabelDelete.setText("You have to type in your password");
            }
            else{
                errorLabelDelete.setText("Provided password is incorrect");
            }
        }

    }
}

package communicator.client.users.controller;

import ch.qos.logback.classic.Logger;
import communicator.client.users.Main;
import communicator.client.users.model.ProfileData;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by Dawid on 2015-12-05.
 */
public class ProfileController implements Controller {
    private Stage primaryStage;
    private Stage profileStage;
    private String userName;
    private ProfileData data;
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass().toString());

    public ProfileController(){}
    public ProfileController(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }
    public ProfileController(Stage primaryStage, String userName,ProfileData data) {
        this.primaryStage = primaryStage;
        this.userName = userName;
        this.data = data;
    }

    @Override
    public void show() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/Profile.fxml"));
            loader.setController(this);
            BorderPane page = (BorderPane) loader.load();

            profileStage = new Stage();
            profileStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    Overseer.getInstance().closingWindow(new ProfileController());
                }
            });
            profileStage.setTitle("User profile");
            profileStage.initModality(Modality.WINDOW_MODAL);
            profileStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            profileStage.setScene(scene);


            profileStage.showAndWait();

        } catch (IOException e) {
            logger.error(e.toString());
        }
    }

    @Override
    public Stage getStage() {
        return profileStage;
    }

    @FXML
    public void initialize(){
        userNameLabel.setText(userName);
        Name.setText(data.getName());
        City.setText(data.getCity());
        Age.setText(data.getAge());
    }

    @FXML
    private Label userNameLabel;
    @FXML
    private Label Name;
    @FXML
    private Label City;
    @FXML
    private Label Age;
}

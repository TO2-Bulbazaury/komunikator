package communicator.client.users.controller;

import javafx.stage.Stage;

/**
 * Created by Dawid on 2015-12-05.
 */
public interface Controller {
    public void  show();
    public Stage getStage();
}

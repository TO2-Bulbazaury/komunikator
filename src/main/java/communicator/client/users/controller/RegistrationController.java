package communicator.client.users.controller;

import ch.qos.logback.classic.Logger;
import communicator.client.users.Main;
import communicator.client.users.model.Model;
import communicator.client.users.model.User;
import communicator.client.users.Main;
import communicator.client.users.model.Model;
import communicator.client.users.model.User;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by Dawid on 2015-12-05.
 */
public class RegistrationController implements Controller {
    private Stage primaryStage;
    private Stage registrationStage;
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass().toString());


    public RegistrationController(){}
    public RegistrationController(Stage primaryStage) {

    }

    @Override
    public void show() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/Registration.fxml"));
            loader.setController(this);
            BorderPane page = (BorderPane) loader.load();

            registrationStage = new Stage();
            registrationStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    Overseer.getInstance().closingWindow(new RegistrationController());
                }
            });
            registrationStage.setTitle("Registration");
            registrationStage.initModality(Modality.WINDOW_MODAL);
            registrationStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            registrationStage.setScene(scene);


            registrationStage.showAndWait();

        } catch (IOException e) {
            logger.error(e.toString());
        }
    }

    @Override
    public Stage getStage() {
        return registrationStage;
    }

    private boolean checkdata(){
        boolean correctData = true;
        if (isFieldEmpty(usernameField)){
            correctData = false;
        }
        if (isFieldEmpty(emailField)){
            correctData = false;
        }
        if (isFieldEmpty(confirmEField)){
            correctData = false;
        }
        if (isFieldEmpty(passwordField)){
            correctData = false;
        }
        if (isFieldEmpty(confirmPField)){
            correctData = false;
        }
        if (!correctData){
            errorLabel.setText("Mandatory data not supplied");
            errorLabel.setTextFill(Color.RED);
            return false;
        }
        if (!emailField.getText().equals(confirmEField.getText())){
            emailField.setStyle("-fx-text-box-border: red;");
            confirmEField.setStyle("-fx-text-box-border: red;");
            errorLabel.setText("Email and confirmation email are not exactly the same");
            errorLabel.setTextFill(Color.RED);
            return false;
        }
        else if(checkEmailFormat()==false){
            emailField.setStyle("-fx-text-box-border: red;");
            confirmEField.setStyle("-fx-text-box-border: red;");
            errorLabel.setText("Bad Email format");
            errorLabel.setTextFill(Color.RED);
            return false;
        }
        else{
            emailField.setStyle("-fx-text-box-border: gray;");
            confirmEField.setStyle("-fx-text-box-border: gray;");
            emailField.setStyle("-fx-text-box-border: gray;");
            confirmEField.setStyle("-fx-text-box-border: gray;");
        }
        if (!passwordField.getText().equals(confirmPField.getText())){
            passwordField.setStyle("-fx-text-box-border: red;");
            confirmPField.setStyle("-fx-text-box-border: red;");
            errorLabel.setText("Password and confirmation password are not exactly the same");
            errorLabel.setTextFill(Color.RED);

            return false;
        }
        else{
            passwordField.setStyle("-fx-text-box-border: black;");
            confirmPField.setStyle("-fx-text-box-border: black;");
            passwordField.setStyle("-fx-text-box-border: gray;");
            confirmPField.setStyle("-fx-text-box-border: gray;");
        }
        errorLabel.setText("");
        return true;
    }

    private boolean checkEmailFormat() {
        if (emailField.getText().length()<5)return false;
        if (!emailField.getText().contains("@")) return false;
        if (!emailField.getText().contains(".")) return false;
        if (emailField.getText().endsWith(".")) return false;
        if (emailField.getText().contains("@."))return false;
        return true;
    }

    private boolean isFieldEmpty(TextField field){
        if(field.getText().isEmpty()){
            field.setStyle("-fx-text-box-border: red;");
            return true;
        }
        field.setStyle("-fx-text-box-border: black;");
        field.setStyle("-fx-text-box-border: gray;");
        return false;
    }


    @FXML
    private Button register;
    @FXML
    private Label usernameLabel;
    @FXML
    private TextField usernameField;
    @FXML
    private Label emailLabel;
    @FXML
    private TextField emailField;
    @FXML
    private Label confirmELabel;
    @FXML
    private TextField confirmEField;
    @FXML
    private Label passwordLabel;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Label confirmPLabel;
    @FXML
    private PasswordField confirmPField;
    @FXML
    private Label nameLabel;
    @FXML
    private TextField nameField;
    @FXML
    private Label cityLabel;
    @FXML
    private TextField cityField;
    @FXML
    private Label ageLabel;
    @FXML
    private TextField ageField;
    @FXML
    private Label errorLabel;

    @FXML
    public void handleSubmitAction(ActionEvent event) {
        if(checkdata()) {
            User user = new User();
            user.setUsername(usernameField.getText());
            user.setPassword(Model.getInstance().getEncryption().encrypt(passwordField.getText()));
            user.getProfileData().setEmail(emailField.getText());
            user.getProfileData().setName(nameField.getText());
            user.getProfileData().setCity(cityField.getText());
            user.getProfileData().setAge(ageField.getText());
            Model.getInstance().setUser(user);
            if(Model.getInstance().register()) {
                /* TODO
                * rejestracja si� powiod�a*/
            } else {
                /* TODO
                * rejestracja si� nie powiod�a*/
            }
        }
    }

}

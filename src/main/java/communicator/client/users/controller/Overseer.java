package communicator.client.users.controller;

import communicator.client.users.model.Model;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dawid on 2015-12-08.
 */
public class Overseer {
    private final static Overseer ourInstance = new Overseer();
    private final List<Controller> controllers = new ArrayList<>();

    private Overseer() {
    }

    public static Overseer getInstance() {
        return ourInstance;
    }


    public void logout(Stage primaryStage){
        closeAllWindows();
        Model.getInstance().logout();
        addController(new LoginController(primaryStage));
    }

    private void closeAllWindows() {
        for(Controller c: controllers){
            c.getStage().close();
        }
        controllers.clear();
    }

    public Controller addController(Controller controller) {
        for(Controller c: controllers){
            if(c.getClass().equals(controller.getClass())){
                c.getStage().toFront();
                return c;
            }
        }
        controllers.add(controller);
        controller.show();
        return controller;
    }

    public void closingWindow(Controller controller) {
        for(Controller c: controllers){
            if(c.getClass().equals(controller.getClass())){
                controllers.remove(c);
                return;
            }
        }
    }
}

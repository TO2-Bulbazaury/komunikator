package communicator.client.users.controller;

import ch.qos.logback.classic.Logger;
import communicator.client.users.Main;
import communicator.client.users.Main;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by Dawid on 2015-12-05.
 */
public class OptionsController implements Controller {
    private Stage primaryStage;
    private Stage optionsStage;
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass().toString());

    public OptionsController(){}
    public OptionsController(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    @Override
    public void show() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/Options.fxml"));
            loader.setController(this);
            BorderPane page = (BorderPane) loader.load();

            optionsStage = new Stage();
            optionsStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    Overseer.getInstance().closingWindow(new OptionsController());
                }
            });
            optionsStage.setTitle("Options");
            optionsStage.initModality(Modality.WINDOW_MODAL);
            optionsStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            optionsStage.setScene(scene);


            optionsStage.show();

        } catch (IOException e) {
            logger.error(e.toString());
        }
    }

    @Override
    public Stage getStage() {
        return optionsStage;
    }

    @FXML
    private Button profile;
    @FXML
    private Button preferences;
    @FXML
    private Button password;
    @FXML
    private Button deacitvate;

    @FXML
    public void handleProfileAction(ActionEvent event) {
        Overseer.getInstance().addController(new ProfileSettingsController(primaryStage));
    }
    @FXML
    public void handlePreferencesAction(ActionEvent event) {
        Overseer.getInstance().addController(new PreferencesController(primaryStage));
    }
    @FXML
    public void handlePasswordAction(ActionEvent event) {
        Overseer.getInstance().addController(new PasswordController(primaryStage));
    }
    @FXML
    public void handleDeactivateAction(ActionEvent event) {
        Overseer.getInstance().addController(new DeactivateController());
    }
}

package communicator.client.users.controller;

import ch.qos.logback.classic.Logger;
import communicator.client.users.Plugin.Plugin;
import communicator.client.users.model.User;
import communicator.client.users.Main;
import communicator.client.users.model.Model;
import communicator.client.users.model.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Dawid on 2015-12-05.
 */
public class PreferencesController implements Controller {

    private Stage primaryStage;
    private Stage preferencessStage;
    private List<Plugin> preferencesList;
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass().toString());

    public PreferencesController() {

    }

    public PreferencesController(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.user = Model.getInstance().getUser();
    }


    @Override
    public void show() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/Preferences.fxml"));
            loader.setController(this);
            BorderPane page = (BorderPane) loader.load();

            preferencessStage = new Stage();
            preferencessStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    Overseer.getInstance().closingWindow(new PreferencesController());
                }
            });
            preferencessStage.setTitle("Preferences");
            preferencessStage.initModality(Modality.WINDOW_MODAL);
            preferencessStage.initOwner(primaryStage);

            Scene scene = new Scene(page);
            preferencessStage.setScene(scene);
            preferencessStage.showAndWait();
        } catch (IOException e) {
            logger.error(e.toString());
        }
    }

    @Override
    public Stage getStage() {
        return preferencessStage;
    }

    private User user;

    @FXML
    private Button submit;
    @FXML
    private ListView<CheckBox> checkboxList;
    private ObservableList<CheckBox> checkBoxes = FXCollections.observableArrayList();


    @FXML
    void addPreference(String name, Boolean preference) {
        CheckBox c = new CheckBox();
        c.setText(name);
        c.setSelected(preference);
        checkBoxes.add(c);
    }

    @FXML
    public void initialize() {
        getAvailablePlugins();

        for(Plugin plugin : preferencesList) {
            addPreference(plugin.getName(), plugin.getPreference());
        }

        checkboxList.setItems(checkBoxes);
    }



    @FXML
    public void handleSubmitAction(ActionEvent event) {
        for(CheckBox c: checkBoxes){
            for(Plugin plugin : preferencesList) {
                if (c.getText().equals(plugin.getName())) {
                    plugin.setPreference(c.isSelected());
                }
            }
        }

        HashMap<String, Boolean> preferences = new HashMap<>();

        for(Plugin plugin : preferencesList) {
            preferences.put(plugin.getName(), plugin.getPreference());
        }

        Model.getInstance().getPluginManager().setPreferences(preferences);
    }

    public void getAvailablePlugins(){
        preferencesList = Model.getInstance().getPluginManager().getPlugins();
    }

}

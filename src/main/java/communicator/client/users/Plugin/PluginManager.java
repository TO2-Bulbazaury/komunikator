package communicator.client.users.Plugin;

import communicator.client.users.Plugin.JarClassLoader;
import communicator.client.users.Plugin.Plugin;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mk on 16/12/15.
 */
public class PluginManager {

    private File PluginsDir;
    private File[] PluginsList;
    private HashMap<String, Boolean> preferences = new HashMap<String, Boolean>();

    ArrayList<Plugin> plugins = new ArrayList<Plugin>();
    ArrayList<Plugin> activePlugins = new ArrayList<Plugin>();

    public PluginManager(String pluginsDir) {
        this.PluginsDir = new File(pluginsDir);
        try {
            init();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private void init() throws MalformedURLException {
        scanPlugins();
        //sort plugins
        for(Plugin p: plugins){
            preferences.put(p.getName(), p.getPreference());
        }
    }

    public String parse(String m){
        activePlugins =  getActivePlugins();
        String tmpMessage = m;
        for(Plugin p: activePlugins){
            tmpMessage = p.parse(tmpMessage);
        }
        return tmpMessage;
    }

    private void scanPlugins() throws MalformedURLException {
        PluginsList = PluginsDir.listFiles();

        for(File f : PluginsList){

            System.out.println("trying to load "+f);

            JarClassLoader jcl = new JarClassLoader(f);
            try {
                plugins.add(jcl.getPlugin());
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public void updatePreferences(String pluginName, Boolean isOn){
        preferences.put(pluginName, isOn);
    }

    public ArrayList<Plugin> getActivePlugins(){
        ArrayList<Plugin> activeOnes = new ArrayList<Plugin>();
        for(Plugin p: plugins){
            if(preferences.get(p.getName())) activeOnes.add(p);
        }
        // sort activeOnes
        return activeOnes;
    }

    public ArrayList<Plugin> getPlugins() {
        return plugins;
    }

    public void setPreferences(HashMap<String, Boolean> newPreferences){
        preferences = newPreferences;
    }

    public HashMap<String, Boolean> getPreferences(){
        return preferences;
    }


}

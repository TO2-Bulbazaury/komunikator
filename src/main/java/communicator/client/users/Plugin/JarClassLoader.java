package communicator.client.users.Plugin;

import communicator.client.users.Plugin.Plugin;
import org.hibernate.annotations.SourceType;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

/**
 * Created by mk on 16/12/15.
 */
public class JarClassLoader {
    private Plugin plugin;
    private String file;

    public JarClassLoader(File f) {
//
        this.file = f.toString();
//        try {
////            init(f.toString());
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    public Plugin getPlugin() throws IOException {

        String pathToJar = this.file;

        JarInputStream jarFile = new JarInputStream(new FileInputStream(pathToJar));
        JarEntry jarEntry;


        URL[] urls = { new URL("jar:file:" + pathToJar+"!/") };
        URLClassLoader cl = URLClassLoader.newInstance(urls);
        while (true) {
            jarEntry = jarFile.getNextJarEntry();
            if (jarEntry == null) {
                break;
            }
            if (jarEntry.getName().endsWith(".class")) {

                String className = jarEntry.getName().substring(0,jarEntry.getName().length()-6);
                System.out.println(className);
                //Class c = cl.loadClass(className);          //note:  classname!!

//                    plugin = (Plugin) Class.forName(className).getConstructor(String.class).newInstance();
                plugin = new DemoPlugin();
                System.out.println(plugin.toString());
                //  System.out.println(o.run());
            }
        }

        return plugin;

    }
}

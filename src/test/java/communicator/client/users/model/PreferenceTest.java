package communicator.client.users.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

/**
 * Created by Jakub Janusz on 2015-12-08.
 */
public class PreferenceTest {

    String name;
    boolean choice;
    User user;
    Preference preference;

    @Before
    public void setUp() {
        preference = new Preference();
        user = mock(User.class);
    }

    @Test
    public void testCreation() {
        assertNull("testCreation - name", preference.getName());
        assertFalse("testCreation - choice", preference.getChoice());
        assertNull("testCreation - user", preference.getUser());
    }

    @Test
    public void testNamePChange() {
        name = "firstPName";
        preference.setName(name);
        assertEquals("testNameChange - first", preference.getName(), name);
        name = "secondPName";
        assertNotEquals("testNameChange - second", preference.getName(), name);
    }

    @Test
    public void testChoiceChange() {
        choice = false;
        preference.setChoice(choice);
        assertFalse("testChoiceChange - false", preference.getChoice());
        choice = true;
        assertNotEquals("testChoiceChange - true", preference.getChoice(), choice);
    }

    @Test
    public void testUserChange() {
        preference = new Preference(user, name, choice);
        assertSame("testUserChange", preference.getUser(), user);
    }

}
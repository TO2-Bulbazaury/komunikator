package communicator.client.users.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Jakub Janusz on 2015-12-08.
 */
public class ProfileDataTest {

    String email;
    String name;
    int age;
    String city;
    ProfileData profileData;

    @Before
    public void setUp() throws Exception {
        profileData = new ProfileData();
    }

    @Test
    public void testCreation() {
        assertNull("testCreation - email", profileData.getEmail());
        assertNull("testCreation - name", profileData.getName());
        assertNull("testCreation - age", profileData.getAge());
        assertNull("testCreation - city", profileData.getCity());
    }

    @Test
    public void testEmailChange() {
        email = "firstEmail";
        profileData.setEmail(email);
        assertEquals("testEmailChange - first", profileData.getEmail(), email);
        email = "secondEmail";
        assertNotEquals("testEmailChange - second", profileData.getEmail(), email);
    }

    @Test
    public void testNameChange() {
        name = "firstName";
        profileData.setName(name);
        assertEquals("testNameChange - first", profileData.getName(), name);
        name = "secondName";
        assertNotEquals("testNameChange - second", profileData.getName(), name);
    }

    @Test
    public void testAgeChange() {
        String age = "18";
        profileData.setAge(age);
        assertEquals("testAgeChange - 18", profileData.getAge(), age);
        age = "21";
        assertNotEquals("testAgeChange - 21", profileData.getAge(), age);
    }

    @Test
    public void testCityChange() {
        city = "firstCity";
        profileData.setCity(city);
        assertEquals("testCityChange - first", profileData.getCity(), city);
        city = "secondCity";
        assertNotEquals("testCityChange - second", profileData.getCity(), city);
    }

}